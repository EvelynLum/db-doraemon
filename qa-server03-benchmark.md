### 1 Table * 1 Million Records

#### Queries performed:
|  | oltp_read_write | oltp_update_non_index | select_random_ranges |
| ---- | ---- | ---- | ---- |
| read | 7346486 | 0 | 5111644 |
| write | 2098996 | 4139788 | 0 |
| other | 1049498 | 0 | 0 |
| total | 10494980 | 4139788 | 5111644 |
| transactions | 524749 (8735.38 per sec.) | 4139788 (68982.86 per sec.) | 5111644 (85164.05 per sec.) |
| queries | 10494980 (174707.55 per sec.) | 4139788 (68982.86 per sec.) | 5111644 (85164.05 per sec.) |
| ignored errors | 0 (0.00 per sec.) | 0 (0.00 per sec.) | 0 (0.00 per sec.) |
| reconnects | 0 (0.00 per sec.) | 0 (0.00 per sec.) | 0 (0.00 per sec.) |

#### General statistics:
|  | oltp_read_write | oltp_update_non_index | select_random_ranges |
| ---- | ---- | ---- | ---- |
| total time | 60.0698s | 60.0100s | 60.0195s |
| total number of events | 524749 | 4139788 | 5111644 |

#### Latency (ms):
|  | oltp_read_write | oltp_update_non_index | select_random_ranges |
| ---- | ---- | ---- | ---- |
| min | 1.76 | 0.12 | 0.20 |
| avg | 11.44 | 1.45 | 1.17 |
| max | 748.85 | 1396.77 | 46.91 |
| 95th percentile | 18.95 | 1.76 | 1.79 |
| sum | 6001347.19 | 5996484.99 | 5989870.13 |

#### Threads fairness:
|  | oltp_read_write | oltp_update_non_index | select_random_ranges |
| ---- | ---- | ---- | ---- |
| events (avg/stddev) | 5247.4900/30.62 | 41397.8800/377.93 | 51116.4400/4507.73 |
| execution time (avg/stddev) | 60.0135/0.03 | 59.9648/0.00 | 59.8987/0.01 |

### 10 Tables * 1 Million Records

#### Queries performed:
|  | oltp_read_write | oltp_update_non_index | select_random_ranges |
| ---- | ---- | ---- | ---- |
| read | 6067376 | 0 | 5602005 |
| write | 1733536 | 4290782 | 0 |
| other | 866768 | 0 | 0 |
| total | 8667680 | 4290782 | 5602005 |
| transactions | 433384 (7219.79 per sec.) | 4290782 (71408.38 per sec.) | 5602005 (93339.16 per sec.) |
| queries | 8667680 (144395.84 per sec.) | 4290782 (71408.38 per sec.) | 5602005 (93339.16 per sec.) |
| ignored errors | 0 (0).00 per sec.) | 0 (0.00 per sec.) | 0 (0.00 per sec.) |
| reconnects | 0 (0.00 per sec.) | 0 (0.00 per sec.) | 0 (0.00 per sec.) |

#### General statistics:
|  | oltp_read_write | oltp_update_non_index | select_random_ranges |
| ---- | ---- | ---- | ---- |
| total time | 60.0253s | 60.0861s | 60.0161s |
| total number of events | 433384 | 4290782 | 5602005 |

#### Latency (ms):
|  | oltp_read_write | oltp_update_non_index | select_random_ranges |
| ---- | ---- | ---- | ---- |
| min | 1.81 | 0.12 | 0.20 |
| avg | 13.84 | 1.40 | 1.07 |
| max | 592.09 | 1391.03 | 35.49 |
| 95th percentile | 27.66 | 1.79 | 1.64 |
| sum | 5998958.54 | 6003610.09 | 5988778.97 |

#### Threads fairness:
|  | oltp_read_write | oltp_update_non_index | select_random_ranges |
| ---- | ---- | ---- | ---- |
| events (avg/stddev) | 4333.8400/49.74 | 42907.8200/590.65 | 56020.0500/5296.13 |
| execution time (avg/stddev) | 59.9896/0.00 | 60.0361/0.02 | 59.8878/0.01 |
