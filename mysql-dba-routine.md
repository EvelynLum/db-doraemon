Create a DBA account
```sql
CREATE USER IF NOT EXISTS 'chinyeelum'@'10.11.%' IDENTIFIED BY 'lalalalalalala';

GRANT CREATE TABLESPACE, CREATE USER, PROCESS, RELOAD, REPLICATION CLIENT, SHUTDOWN, SUPER ON *.* TO 'chinyeelum'@'10.11.%';
GRANT SELECT ON information_schema.* TO 'chinyeelum'@'10.11.%';
GRANT SELECT ON performance_schema.* TO 'chinyeelum'@'10.11.%';
GRANT LOCK TABLES, SELECT ON mysql.* TO 'chinyeelum'@'10.11.%';
GRANT SELECT ON sys.* TO 'chinyeelum'@'10.11.%';

FLUSH PRIVILEGES;
```

```sql
GRANT ALL PRIVILEGES ON *.* TO 'chinyeelum'@'%' [WITH GRANT OPTION];
```

```sql
SELECT * FROM mysql.user;
SHOW GRANTS FOR 'user'@'localhost';
```

Update user password
```sql
UPDATE mysql.user SET authentication_string = PASSWORD('NEW_USER_PASSWORD')WHERE User = 'user-name' AND Host = 'localhost';
FLUSH PRIVILEGES;
```

Dump user
```sql
mysqldump -h192.168.101.65 -uchinyeelum -p mysql user > /tmp/user_dump.sql
```
(With SSH into the machine)
```sql
mysqldump -uchinyeelum -p -S /var/run/mysqld/mysqld-3306.sock mysql --single-transaction > /tmp/user_dump.sql
```

ALTER mysql.user table miscolumn mismatched due to mysql version
```sql
ALTER TABLE `mysql`.`user`
ADD COLUMN Password_require_current enum('N','Y') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
ADD COLUMN User_attributes json DEFAULT NULL;
```