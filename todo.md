Save slow log query log in table in ansible role (maybe for DEV, QA, INT env).

Send slow log query to Ken and Joe [account-to-send](https://www.sige.la/group/PG_STRESS_TEST?msg=ywLe9b46xbLJC4b7R).

Optimize percona role, all sections except Innodb section.

[Add mutex contention dashboard](https://grafana.com/grafana/dashboards/9892)

[Add custom queries](https://www.percona.com/blog/2019/03/12/pmms-custom-queries-in-action-adding-a-graph-for-innodb-mutex-waits/)