After ssh to PostgreSQL server, switch to `postgres` user.

`su - postgres`

Create new user
`create user csqa_dev with password 'hCB}9@@>';`

Grant 
`GRANT ALL PRIVILEGES ON DATABASE csqa_20190622 TO csqa_dev;`
`GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO csqa_dev;`

[Reference](https://dba.stackexchange.com/questions/33943/granting-access-to-all-tables-for-a-user)


-- Convert 
```sql
SELECT TO_TIMESTAMP(1565339947);
```

-- To convert back to unix timestamp you can use date_part:
```sql
SELECT DATE_PART('epoch',CURRENT_TIMESTAMP)::int;
```
```sql
SELECT timezone('Europe/London', NOW()::timestamptz);
```

Vacuum
```sql
DELETE FROM public.goflow_records WHERE last_switched < (NOW() - interval '3 day');
VACUUM VERBOSE eve_schema.const_enum;

VACUUM FULL eve_schema.const_enum;
```