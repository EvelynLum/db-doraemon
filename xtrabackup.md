# Percona Xtrabackup

[Percona Xtrabackup Documentation.](https://www.percona.com/doc/percona-xtrabackup/8.0/index.html)

Percona Xtrabackup consist of:

1. [Full Backup](https://www.percona.com/doc/percona-xtrabackup/8.0/backup_scenarios/full_backup.html)
1. [Incremental Backup](https://www.percona.com/doc/percona-xtrabackup/8.0/backup_scenarios/incremental_backup.html)
1. [Streaming Backup](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/backup.streaming.html)
1. [Compressed Backup](https://www.percona.com/doc/percona-xtrabackup/8.0/backup_scenarios/compressed_backup.html)

## Full Backup

```
xtrabackup --user=xtrabackup_user --password=password123 -S /var/run/mysqld/mysqld-3306.sock --backup --target-dir=/data/backups/
```

To create a backup, run xtrabackup with the `--backup` option.
Specify the `--target-dir` option, which mean where your backup-ed files wll be stored.

Preparing a backup.

After making a backup with the --backup option, you need to `prepare` it in order to restore with the `--prepare` option.

```
xtrabackup --prepare --target-dir=/data/backups/
```

## Incremental Backup

To make an incremental backup, begin with a full backup as usual.  It copies only the data that has changed since the last full backup.

### Creating an Incremental Backup

To make an incremental backup, begin with a full backup ausual. The xtrabackup binary writes a file called `xtrabackup_checkpoints` into the backup's target directory.

Step 1:

Create a full backup like above:

```
xtrabackup --user=xtrabackup_user --password=password123 -S /var/run/mysqld/mysqld-3306.sock --backup --target-dir=/data/backups/
```

Step 2:

Start incremental backup that the base directory points to `/data/backups/`

```
xtrabackup --user=xtrabackup_user --password=password123 -S var/run/mysqld/mysqld-3306.sock --backup --target-dir=/data/incremental --incremental-basedir=/data/backups/
```

Preparing the Incremental Backups

The `--prepare` step for incremental backups is not the same as for full backups and you should use the `--apply-log-only` option to prevent the rollback phase.

```
xtrabackup --prepare --apply-log-only --target-dir=/data/backups/
--incremental-dir=/data/backups/inc1
```

## Streaming Backup

[Reference](https://www.percona.com/forums/questions-discussions/percona-xtrabackup/51705-mysql-backup-of-remote-server-in-my-local-machine-using-percona-xtrabackup)

Streaming backup is chosen because:
It is able to backup from remote server to another server through a`STDOUT file` in the xbstream format instead of copying files to the backup directory.

The option used is `--stream=xbstream`

How streaming backup works???

Let's say we use old and new DEV server.

On `new Dev` server:

Step 1:

Creare a directory on where STDOUT files will be stored. In this case, it will store in `/data/mysql/backup/`. And the log file will be display at `backup.log`

```
ssh root@10.13.1.18 "xtrabackup --user=xtrabackup_user --password=password123 -S /var/run/mysqld/mysqld-3306.sock --backup --compress --compress-threads=8 --parallel=4  --stream=xbstream" > /data/mysql/backup/backup.xbstream  2> backup.log
```

Options descriptions:
--compress-thread -> additionally speed up the compression process with the --compress-threads option.  This  option  specifies  the number of threads created by xtrabackup for for parallel data compression. The default value for this option is 1.

--parallel -> multiple files can be copied at the same time when using the this option. This option specifies the number of threads created by xtrabackup to copy data files.

Step 2:

After backup sucessfully, there will be a `backup.xbstream` files in the backup directory. You need to extract the files with `-x` option.

```
xbstream -x --decompress < backup.xbstream
```

Step 3:

Prepare and restore.

```
xtrabackup --prepare --apply-log-only --target-dir=/data/mysql/backup/
```

Note:
If you want to test on fail-over, simply:
1. Stop mysql service, `systemctl stop mysqld@3306`
1. Change datadir name, `mv 3306 3306-ori`
1. Change ownerhip of the backup directory, `chown -R mysql:mysql /data/mysql/backup
1. Change backup directory name to 3306, `mv backup 3306`
1. Lastly, start the service, `systemctl start mysqld@3306`

## Compressed backup

Skipped =D
