# Quick Access 我的名字叫索引

:elephant: [PostgreSQL Maintenance Page](postgres.md)

- [MySQL Calculator](https://www.mysqlcalculator.com/)


- Next cloud pt-archiver file

    https://nextcloud.cirosolution.com/index.php/apps/files/?dir=/pt-archiver&fileid=1111362#editor

- Grafana Monitoring

    https://dev-qa-grafana.cirosolution.com

    https://vrf-int-grafana.cirosolution.com

    https://tw-prod-grafana.cirosolution.com

- Markdown

    [Gitlab Markdown](https://docs.gitlab.com/ee/user/markdown.html)

    [Markdown Emoji](https://www.webfx.com/tools/emoji-cheat-sheet/)

- Reading Materials

    [码匠](https://www.oicto.com/category/mysql/)


# Urgent to-do tasks 我要快快搞清楚

:exclamation: Attention :exclamation:

| Tag | Link | Remarks |
| ------ | ------ | ------ |
| Context switch | https://www.percona.com/blog/2017/11/09/mysql-linux-context-switches/ | Performance tuning |
| Drop large table | https://dbtut.com/index.php/2018/08/07/mysql-how-to-drop-a-large-table-gracefully/ | Write the full steps on how to do it in CIRO server |
| Connection handling | https://mysqlserverteam.com/mysql-connection-handling-and-scaling/ | |
| PMM Dashboard | https://pmmdemo.percona.com/graph/d/MQWgroiiz/mysql-overview?refresh=1m&orgId=1 | Refer and apply to current dashboard |
| Speedy MySQL tuning configuration | https://www.youtube.com/watch?v=0CqMv0ucqFA | [my.cnf with config recommendations](http://speedemy.com/files/mysql/my.cnf) <br /> [17 keys for MySQL tuning](http://www.speedemy.com/17-key-mysql-config-file-settings-mysql-5-7-proof/) |


# Handy piece of knowledge 我的叮当法宝

Check server hard disk size
`du /data/live -cah -d 1 | sort -h | grep _db`

-- Version 8.0
```sql
SELECT Name, FS_BLOCK_SIZE, FILE_SIZE, ALLOCATED_SIZE 
FROM INFORMATION_SCHEMA.INNODB_TABLESPACES;
```
-- Version 5.7

5.7 update medata periodically, low accuracy.
```sql
SELECT *
FROM INFORMATION_SCHEMA.INNODB_TABLES;
```

Reclaim hard disk space
`DROP TABLE` will reclaim hard disk space immediately.

After `TRUNCATE` table or `DELETE` rows, it won't release hard disk space, need to run `OPTIMIZE`.
```sql
OPTIMIZE TABLE ganeshagold_db.ganeshagold_job_log;
```

If running really low disk space, flush logs can help to reduce
```sql
FLUSH LOGS;
```

This variable has to set to a directory as data output file
```sql
SHOW VARIABLES LIKE "secure_file_priv";
```

(Without header) Export into file and load data [LOAD DATA](https://dev.mysql.com/doc/refman/8.0/en/load-data.html)
```sql
SELECT * FROM `pg_bet_db`.`bet` WHERE `bet_status` == 1
INTO OUTFILE '/var/lib/mysql-files/failed_bet';
```

Load data into table
```sql
LOAD DATA INFILE '/var/lib/mysql-files/failed_bet'
    REPLACE -- specify to override existing data with the data in file when duplicate-key occurs
    INTO TABLE pg_bet_db.bet_tmp_13
    IGNORE 1 LINES; -- specify to IGNORE 1st LINE which is header
```

*Sample below also shows use case 'Migrate old table to new table with a new column where data (of new column) comes from another table'*

(With header) Export into file and load data [LOAD DATA](https://dev.mysql.com/doc/refman/8.0/en/load-data.html)
```sql
SELECT 'id', 'amount', 'nullable_remarks', 'create_time'
UNION
SELECT eu.id, amount, nullable_remarks, bet.create_time FROM `blackjack_eu_db`.`eve_load_test`eu
INNER JOIN `pg_bet_db`.`bet` bet ON eu.id=bet.id
INTO OUTFILE '/var/lib/mysql-files/load_test_with_header_join_dt';
```

Load data into table
```sql
LOAD DATA INFILE '/var/lib/mysql-files/load_test_with_header_join_dt'
    REPLACE -- specify to override existing data with the data in file when duplicate-key occurs
    INTO TABLE `blackjack_eu_db`.`eve_load_test`
    IGNORE 1 LINES; -- specify to IGNORE 1st LINE which is header
```

Create index with `INPLACE` algorithm
```sql
CREATE INDEX operator_wallet_transaction_create_time
USING BTREE ON pg_operator_middleware_db.operator_wallet_transaction (create_time)
ALGORITHM=INPLACE;
```

`SELECT` with Force index
```sql
SELECT *
FROM `diaochan_db`.`diaochan_spin` 
FORCE INDEX(`diaochan_spin_ix_spin_status_update_time`) 
WHERE spin_status = 1 AND update_time < TIMESTAMP(DATE_SUB(NOW(),INTERVAL 180 DAY));
```

`SELECT COUNT` faster with `using index` only, without `using where`

Try `EXPLAIN` query below, it shows only `using index` only.

[Reference](https://www.oreilly.com/library/view/high-performance-mysql/9780596101718/ch04.html)

Find `In general, MySQL can apply a WHERE clause in three ways, from best to worst:`
```sql
SELECT COUNT(DISTINCT(id))
FROM `blackjack_us_db`.`blackjack_us_spin`;
```

`CONCAT` as utility
```sql
SELECT CONCAT('\'', COLUMN_NAME, '\',') FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='blackjack_eu_db' AND TABLE_NAME='eve_load_test';
```

`CONVERT` data type
```sql
SELECT CONVERT(id, CHAR(64)) FROM pg_bet_db.bet;
```

Show table lock
```sql
SHOW OPEN TABLE; -- LIKE 'bet'
```

Drop table without metadata lock
```sql
ALTER TABLE db.tbl RENAME db.tbl_tmp;
TRUNCATE TABLE db.tbl_tmp;
DROP TABLE db.tbl_tmp;
```

Show threads
```sql
SELECT * FROM performance_schema.threads;
```

Show partitions
```sql
SELECT * from information_schema.PARTITIONS WHERE TABLE_SCHEMA = 'pg_bet_db';
```
